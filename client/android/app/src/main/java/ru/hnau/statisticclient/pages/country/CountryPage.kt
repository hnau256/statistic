package ru.hnau.statisticclient.pages.country

import android.content.Context
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.statistic_common.country.Country
import ru.hnau.statisticclient.R
import ru.hnau.statisticclient.pages.map.MapPage
import ru.hnau.statisticclient.utils.ColorManager
import ru.hnau.statisticclient.utils.LinesMapDrawable
import ru.hnau.statisticclient.view.map.MapViewCountryInfo


class CountryPage(
        private val country: Country
) : Page<CountryPageView>() {

    companion object {

        private val THIS_COUNTRY_COLOR = ColorGetter.byResId(R.color.map_selected_country)
        private val OTHER_COUNTRY_COLOR = ColorGetter.byResId(R.color.map_country_fill)

    }

    override fun generateView(context: Context) =
            CountryPageView(
                    context = context,
                    country = country,
                    onShowOnMapButtonClicked = this::onShowOnMapButtonClicked,
                    onBackButtonClicked = this::onBackButtonClicked
            )

    private fun onShowOnMapButtonClicked(context: Context) {
        showPage(
                MapPage(
                        title = StringGetter(country.nameRu),
                        countriesInfo = getCountriesInfo(context)
                )
        )
    }

    private fun getCountriesInfo(context: Context) =

            Country.values().associate { country ->

                val color = if (country == this.country) THIS_COUNTRY_COLOR else OTHER_COUNTRY_COLOR
                val drawable = LinesMapDrawable(
                        context = context,
                        textColor = ColorManager.BG_DARK,
                        lines = listOfNotNull(country.nameRu)
                )

                country to MapViewCountryInfo(
                        color = color,
                        drawable = drawable
                )
            }

    private fun onBackButtonClicked() {
        goBack()
    }

}