package ru.hnau.statisticclient.utils

import ru.hnau.statistic_common.measurement.Measurement


val Measurement.valueWithDimension: String
    get() = value + " " + category.dimension.title

val Measurement.doubleValue: Double
    get() = (if (category.realValue) floatValue?.toDouble() else longValue?.toDouble()) ?: 0.0