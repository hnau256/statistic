package ru.hnau.statisticclient.pages.main.view

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.jutils.producer.DataProducer
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import ru.hnau.statisticclient.R


enum class MainPageListType(
        titleResId: Int
) {

    COUNTRY(R.string.main_page_title_countries),
    CATEGORY(R.string.main_page_title_categories);

    val title = StringGetter(titleResId)

    fun getItemsProducer(
            onCountrySelected: (Country) -> Unit,
            onCategorySelected: (Category) -> Unit
    ) =
            DataProducer(
                    when (this) {
                        COUNTRY ->
                            Country.values().sortedBy { it.nameRu }.map { country ->
                                MainPageListItem(
                                        title = StringGetter(country.nameRu),
                                        info = StringGetter(country.codeEn2),
                                        onClick = { onCountrySelected.invoke(country) }
                                )
                            }
                        CATEGORY ->
                            Category.values().sortedBy { it.title }.map { category ->
                                MainPageListItem(
                                        title = StringGetter(category.title),
                                        info = StringGetter(category.dimension.title),
                                        onClick = { onCategorySelected.invoke(category) }
                                )
                            }
                    }
            )

}