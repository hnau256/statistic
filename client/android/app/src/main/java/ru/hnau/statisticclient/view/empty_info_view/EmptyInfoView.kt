package ru.hnau.statisticclient.view.empty_info_view

import android.content.Context
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.ripple.RippleDrawInfo
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.LinearLayoutSeparator
import ru.hnau.androidutils.ui.view.clickable.ClickableLabel
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.statisticclient.utils.ColorManager


class EmptyInfoView(
        context: Context,
        title: StringGetter? = null,
        subtitle: StringGetter? = null,
        button: EmptyInfoViewButtonInfo? = null
) : LinearLayout(context) {

    companion object {

        private val TITLE_LABEL_INFO = LabelInfo(
                textColor = ColorManager.FG,
                textSize = DpPxGetter.fromDp(20),
                gravity = HGravity.CENTER
        )

        private val SUBTITLE_LABEL_INFO = LabelInfo(
                textColor = ColorManager.FG_50,
                textSize = DpPxGetter.fromDp(16),
                gravity = HGravity.CENTER
        )

        private val BUTTON_LABEL_INFO = LabelInfo(
                textColor = ColorManager.PRIMARY,
                textSize = DpPxGetter.fromDp(16),
                gravity = HGravity.CENTER
        )

        private val BUTTON_RIPPLE_INFO = RippleDrawInfo(
                backgroundColor = ColorManager.SELECT,
                color = ColorManager.PRIMARY
        )

        private val MARGIN_BOTTOM = DpPxGetter.fromDp(12)

    }

    private val titleView = title?.let {
        Label(
                context = context,
                info = TITLE_LABEL_INFO,
                initialText = it
        ).apply {
            setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
                setBottomMargin(context, MARGIN_BOTTOM)
            }
        }
    }

    private val subtitleView = subtitle?.let {
        Label(
                context = context,
                info = SUBTITLE_LABEL_INFO,
                initialText = it
        ).apply {
            setLinearLayoutLayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
                setBottomMargin(context, MARGIN_BOTTOM)
            }
        }
    }

    private val buttonView = button?.let { buttonInfo ->
        ClickableLabel(
                context = context,
                initialText = buttonInfo.title,
                info = BUTTON_LABEL_INFO,
                rippleDrawInfo = BUTTON_RIPPLE_INFO
        ).apply {
            val paddingH = DpPxGetter.fromDp(16)
            setPadding(paddingH, DpPxGetter.fromDp(12), paddingH, DpPxGetter.fromDp(8))
            setOnClickListener { buttonInfo.onClick.invoke() }
        }
    }

    init {
        orientation = HORIZONTAL

        addView(LinearLayoutSeparator(context, 1f))
        addView(
                LinearLayout(context).apply {
                    orientation = VERTICAL
                    gravity = Gravity.CENTER
                    setLinearLayoutLayoutParams(0, MATCH_PARENT, 1f)

                    titleView?.let { addView(it) }
                    subtitleView?.let { addView(it) }
                    buttonView?.let { addView(it) }

                }
        )
        addView(LinearLayoutSeparator(context, 1f))

    }


}