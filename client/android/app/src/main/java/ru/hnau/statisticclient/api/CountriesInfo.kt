package ru.hnau.statisticclient.api

import ru.hnau.jutils.cache.AsyncAutoPossibleCache
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.measurement.Measurement


object CountriesInfo: AsyncAutoPossibleCache<Country, List<Measurement>>(
        capacity = Country.values().size,
        getter = Api.Companion::countryInfo
)