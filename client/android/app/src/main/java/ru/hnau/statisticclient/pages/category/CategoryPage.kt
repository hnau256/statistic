package ru.hnau.statisticclient.pages.category

import android.content.Context
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.measurement.Measurement
import ru.hnau.statisticclient.R
import ru.hnau.statisticclient.pages.map.MapPage
import ru.hnau.statisticclient.utils.ColorManager
import ru.hnau.statisticclient.utils.LinesMapDrawable
import ru.hnau.statisticclient.utils.doubleValue
import ru.hnau.statisticclient.utils.valueWithDimension
import ru.hnau.statisticclient.view.map.MapViewCountryInfo


class CategoryPage(
        private val category: Category
) : Page<CategoryPageView>() {

    companion object {

        private val MIN_VALUE_COLOR = ColorGetter.byResId(R.color.map_min_value)
        private val MAX_VALUE_COLOR = ColorGetter.byResId(R.color.map_max_value)
        private val NO_VALUE_COLOR = ColorGetter.byResId(R.color.map_country_fill)

    }

    override fun generateView(context: Context) =
            CategoryPageView(
                    context = context,
                    category = category,
                    onShowOnMapButtonClicked = this::onShowOnMapButtonClicked,
                    onBackButtonClicked = this::onBackButtonClicked
            )

    private fun onShowOnMapButtonClicked(context: Context, measurements: List<Measurement>) {
        showPage(
                MapPage(
                        title = StringGetter(category.title),
                        countriesInfo = getCountriesInfo(context, measurements)
                )
        )
    }

    private fun getCountriesInfo(context: Context, measurements: List<Measurement>): Map<Country, MapViewCountryInfo> {

        val measurementsCount = measurements.count()

        if (measurementsCount <= 0) {
            return emptyMap()
        }

        val countriesWithColors =
                measurements
                        .sortedBy { it.doubleValue }
                        .mapIndexed { i, measurement ->
                            val minToMaxPosition = i.toFloat() / measurementsCount.toFloat()
                            val color = MIN_VALUE_COLOR.mapInterThisAndOther(MAX_VALUE_COLOR, minToMaxPosition)
                            measurement.country to (measurement to color)
                        }.associate { it }

        return Country.values().associate { country ->
            val (measurement, color) = countriesWithColors[country] ?: (null to NO_VALUE_COLOR)
            val drawable = LinesMapDrawable(
                    context = context,
                    textColor = ColorManager.BG_DARK,
                    lines = listOfNotNull(country.nameRu, measurement?.valueWithDimension)
            )
            country to MapViewCountryInfo(
                    color = color,
                    drawable = drawable
            )
        }

    }


    private fun onBackButtonClicked() {
        goBack()
    }

}