package ru.hnau.statisticclient.view.list_loader

import android.annotation.SuppressLint
import android.content.Context
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.drawables.waiter.WaiterDrawableColor
import ru.hnau.androidutils.ui.drawables.waiter.WaiterDrawableSize
import ru.hnau.androidutils.ui.drawables.waiter.WaiterView
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.base_list.BaseList
import ru.hnau.androidutils.ui.view.base_list.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setFrameLayoutLayoutParams
import ru.hnau.androidutils.ui.view.view_changer.ViewChanger
import ru.hnau.androidutils.ui.view.view_changer.ViewChangerInfo
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.await
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.FinishersLockedProducer
import ru.hnau.statisticclient.R
import ru.hnau.statisticclient.utils.ColorManager
import ru.hnau.statisticclient.view.empty_info_view.EmptyInfoView
import ru.hnau.statisticclient.view.empty_info_view.EmptyInfoViewButtonInfo


@SuppressLint("ViewConstructor")
class ListLoader<T : Any>(
        context: Context,
        private val contentFinishersGetter: () -> Finisher<Possible<List<T>>>,
        itemsViewsWrappersCreator: () -> BaseListViewWrapper<T>
) : FrameLayout(
        context
) {

    private val lockedProducer = FinishersLockedProducer()

    private val waiterView = WaiterView(
            context = context,
            color = ColorManager.DEFAULT_WAITER_COLOR,
            size = ColorManager.DEFAULT_WAITER_SIZE,
            lockedProducer = lockedProducer
    )

    private val contentSwitcher = ViewChanger(
            context = context,
            info = ViewChangerInfo(
                    scrollFactor = 0.5f,
                    fromSide = Side.BOTTOM
            )
    )

    val itemsProducer = ListLoaderItemsProducer<T>()

    private val list = BaseList(
            context = context,
            itemsProducer = itemsProducer,
            viewWrappersCreator = itemsViewsWrappersCreator
    )

    private val errorWhileLoadingInfoView by lazy {
        EmptyInfoView(
                context = context,
                title = StringGetter(R.string.list_loader_error),
                button = EmptyInfoViewButtonInfo(
                        title = StringGetter(R.string.list_loader_reload).toUpperCase(),
                        onClick = this::loadList
                )
        )
    }

    init {
        addView(contentSwitcher)
        addView(waiterView)
        loadList()
    }

    private fun loadList() {
        contentFinishersGetter.invoke()
                .useLockedProducer(lockedProducer)
                .await(
                        onSuccess = this::onItemsReceived,
                        onError = this::onErrorWhileLoading
                )
    }

    private fun onItemsReceived(items: List<T>) {
        contentSwitcher.showView(list)
        itemsProducer.items = items
    }

    private fun onErrorWhileLoading(exception: Exception?) {
        contentSwitcher.showView(errorWhileLoadingInfoView)
    }


}