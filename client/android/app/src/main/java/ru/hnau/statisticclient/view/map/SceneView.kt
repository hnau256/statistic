package ru.hnau.statisticclient.view.map

import android.content.Context
import android.graphics.*
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement


abstract class SceneView(
        context: Context,
        private val sceneWidth: Float,
        private val sceneHeight: Float,
        maxScale: Float = 100f
) : CameraView(
        context = context,
        minScale = 1f,
        maxScale = maxScale
) {

    private val sceneAspectRatio = sceneWidth / sceneHeight

    private var sceneScale = 1f
    private var sceneOffset = PointF()

    private var scaledViewport = RectF()

    final override fun drawContent(canvas: Canvas, viewport: Rect) {
        canvas.save()
        canvas.scale(sceneScale, sceneScale)
        canvas.translate(sceneOffset.x, sceneOffset.y)

        scaledViewport.set(
                viewport.left.toFloat() / sceneScale - sceneOffset.x,
                viewport.top.toFloat() / sceneScale - sceneOffset.y,
                viewport.right.toFloat() / sceneScale - sceneOffset.x,
                viewport.bottom.toFloat() / sceneScale - sceneOffset.y
        )

        drawScene(canvas, scaledViewport)

        canvas.restore()
    }

    protected abstract fun drawScene(canvas: Canvas, viewport: RectF)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getMaxMeasurement(widthMeasureSpec, 0)
        val height = getMaxMeasurement(heightMeasureSpec, 0)
        setMeasuredDimension(width, height)

        if (width <= 0 || height <= 0) {
            return
        }

        val aspectRatio = width.toFloat() / height.toFloat()

        sceneScale = if (aspectRatio > sceneAspectRatio) {
            height.toFloat() / sceneHeight
        } else {
            width.toFloat() / sceneWidth
        }
        sceneOffset.set(
                (width / sceneScale - sceneWidth) / 2f,
                (height / sceneScale - sceneHeight) / 2f
        )
    }


}