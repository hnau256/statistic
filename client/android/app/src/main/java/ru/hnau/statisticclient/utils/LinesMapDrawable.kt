package ru.hnau.statisticclient.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.statisticclient.view.map.MapDrawable


class LinesMapDrawable(
        context: Context,
        private val lines: List<String>,
        textColor: ColorGetter = ColorGetter.BLACK
) : MapDrawable() {

    companion object {

        private const val BASE_TEXT_SIZE = 100f

    }

    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = textColor.get(context)
        textAlign = Paint.Align.CENTER
        textSize = BASE_TEXT_SIZE
    }

    private val lineHeight = calcLineHeight()

    init {
        textPaint.textSize = lineHeight
    }

    private fun calcLineHeight(): Float {
        val maxLineWidth = getMaxLineWidth()
        if (maxLineWidth == null || maxLineWidth <= 0) {
            return 0f
        }
        return BASE_TEXT_SIZE * MapDrawable.BASE_SCALE / maxLineWidth
    }

    private fun getMaxLineWidth() =
            lines.map(textPaint::measureText).max()

    override fun draw(canvas: Canvas) {
        val linesCount = lines.size.takeIf { it > 0 } ?: return
        val offsetY = lineHeight - (linesCount.toFloat() / 2f) * lineHeight
        lines.forEachIndexed { i, line ->
            canvas.drawText(line, 0f, i * lineHeight + offsetY, textPaint)
        }
    }
}