package ru.hnau.statisticclient.api

import ru.hnau.jutils.cache.AsyncAutoPossibleCache
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.measurement.Measurement


object CategoriesInfo : AsyncAutoPossibleCache<Category, List<Measurement>>(
        capacity = Category.values().size,
        getter = Api.Companion::categoryInfo
)