package ru.hnau.statisticclient.pages.main.view

import ru.hnau.androidutils.context_getters.StringGetter


data class MainPageListItem(
        val title: StringGetter,
        val info: StringGetter,
        val onClick: () -> Unit
)