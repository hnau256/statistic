package ru.hnau.statisticclient.pages.main

import android.content.Context
import ru.hnau.androidutils.ui.view.pager.Page
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import ru.hnau.statisticclient.pages.category.CategoryPage
import ru.hnau.statisticclient.pages.country.CountryPage
import ru.hnau.statisticclient.pages.main.view.MainPageView


class MainPage : Page<MainPageView>() {

    override fun generateView(context: Context) =
            MainPageView(
                    context = context,
                    onCountrySelected = this::onCountrySelected,
                    onCategorySelected = this::onCategorySelected
            )

    private fun onCountrySelected(country: Country) {
        showPage(CountryPage(country))
    }

    private fun onCategorySelected(category: Category) {
        showPage(CategoryPage(category))
    }

}