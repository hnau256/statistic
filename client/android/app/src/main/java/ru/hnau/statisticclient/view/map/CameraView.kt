package ru.hnau.statisticclient.view.map

import android.content.Context
import android.graphics.Canvas
import android.graphics.PointF
import android.graphics.Rect
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View


abstract class CameraView(
        context: Context,
        private val minScale: Float = 0.1f,
        private val maxScale: Float = 100f
) : View(context) {

    private var scale: Float = 1f
    private var offset = PointF()

    private var viewport = Rect()

    private val gesture = GestureDetector(context, object : GestureDetector.OnGestureListener {

        override fun onShowPress(e: MotionEvent?) {}
        override fun onDown(e: MotionEvent) = false
        override fun onFling(e1: MotionEvent, e2: MotionEvent, vx: Float, vy: Float) = false
        override fun onLongPress(e: MotionEvent) {}
        override fun onSingleTapUp(e: MotionEvent) = false

        override fun onScroll(e1: MotionEvent, e2: MotionEvent, dx: Float, dy: Float): Boolean {
            offset.x += dx / scale
            offset.y += dy / scale
            invalidate()
            return true
        }

    })

    private val scaleGesture = ScaleGestureDetector(context, object : ScaleGestureDetector.OnScaleGestureListener {
        override fun onScaleBegin(detector: ScaleGestureDetector) = true
        override fun onScaleEnd(detector: ScaleGestureDetector) {
            scale = Math.min(Math.max(scale * detector.scaleFactor, minScale), maxScale)
            invalidate()
        }

        override fun onScale(detector: ScaleGestureDetector): Boolean {
            val scaleFactor = detector.scaleFactor
            if (scaleFactor < 0.01) {
                return false
            }

            val fx = detector.focusX
            val fy = detector.focusY

            offset.x += fx / scale
            offset.y += fy / scale

            scale = Math.min(Math.max(scale * scaleFactor, minScale), maxScale)

            offset.x -= fx / scale
            offset.y -= fy / scale

            invalidate()
            return true
        }
    })

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        canvas.save()
        canvas.scale(scale, scale)
        canvas.translate(-offset.x, -offset.y)

        viewport.set(
                offset.x.toInt(),
                offset.y.toInt(),
                (offset.x + width / scale).toInt(),
                (offset.y + height / scale).toInt()
        )
        drawContent(canvas, viewport)

        canvas.restore()

    }

    protected abstract fun drawContent(canvas: Canvas, viewport: Rect)

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val scrollResult = gesture.onTouchEvent(event)
        return scaleGesture.onTouchEvent(event) || scrollResult
    }

}