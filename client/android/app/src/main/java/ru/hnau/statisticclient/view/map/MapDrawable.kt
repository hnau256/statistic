package ru.hnau.statisticclient.view.map

import android.graphics.Canvas


abstract class MapDrawable {

    companion object {

        const val BASE_SCALE = 1000f

    }

    abstract fun draw(canvas: Canvas)

}