package ru.hnau.statisticclient.pages.views

import android.content.Context
import android.view.ContextMenu
import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.header.HeaderTitle
import ru.hnau.androidutils.ui.view.header.back.HeaderWithBackButton
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.statisticclient.utils.ColorManager


open class PageViewWithHeader(
        context: Context,
        title: StringGetter,
        onBackClicked: () -> Unit,
        contentView: View
) : LinearLayout(context) {

    private val header = HeaderWithBackButton(
            context = context,
            headerInfo = ColorManager.DEFAULT_HEADER_INFO,
            rippleDrawInfo = ColorManager.DEFAULT_RIPPLE_DRAW_INFO,
            backButtonInfo = ColorManager.DEFAULT_BACK_BUTTON_INFO,
            onBackClicked = onBackClicked
    ).apply {

        setLinearLayoutLayoutParams(MATCH_PARENT, WRAP_CONTENT) {
            setBottomMargin(context, DpPxGetter.fromDp(1))
        }

        addView(
                HeaderTitle(
                        context = context,
                        textColor = ColorManager.FG,
                        initialText = title
                )
        )

    }

    init {
        orientation = VERTICAL
        addView(header)
        addView(contentView)
        setBackgroundColor(ColorManager.BG_DARK)
    }

}