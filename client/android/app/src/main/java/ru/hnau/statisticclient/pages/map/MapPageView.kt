package ru.hnau.statisticclient.pages.map

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.statistic_common.country.Country
import ru.hnau.statisticclient.pages.views.PageViewWithHeader
import ru.hnau.statisticclient.view.map.MapViewCountryInfo


@SuppressLint("ViewConstructor")
class MapPageView(
        context: Context,
        title: StringGetter,
        onBackButtonClicked: () -> Unit,
        countriesInfo: Map<Country, MapViewCountryInfo>
) : PageViewWithHeader(
        context = context,
        contentView = MapPageInnerView(context, countriesInfo),
        onBackClicked = onBackButtonClicked,
        title = title
)