package ru.hnau.statisticclient.pages.map

import android.annotation.SuppressLint
import android.content.Context
import android.widget.FrameLayout
import ru.hnau.androidutils.ui.drawables.waiter.WaiterView
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.utils.getMaxMeasurement
import ru.hnau.androidutils.ui.view.utils.makeMeasureSpec
import ru.hnau.androidutils.ui.view.view_changer.ViewChanger
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.locked_producer.AlwaysLockedProducer
import ru.hnau.statistic_common.country.Country
import ru.hnau.statisticclient.utils.ColorManager
import ru.hnau.statisticclient.utils.CountriesMapManager
import ru.hnau.statisticclient.view.map.MapView
import ru.hnau.statisticclient.view.map.MapViewCountryInfo


@SuppressLint("ViewConstructor")
class MapPageInnerView(
        context: Context,
        countriesInfo: Map<Country, MapViewCountryInfo>
) : ViewChanger(
        context = context,
        scrollFactor = 0.5f,
        fromSide = Side.BOTTOM
) {

    private val waiterViewContainer = object : FrameLayout(context) {

        init {
            addView(
                    WaiterView(
                            context = context,
                            color = ColorManager.DEFAULT_WAITER_COLOR,
                            size = ColorManager.DEFAULT_WAITER_SIZE,
                            lockedProducer = AlwaysLockedProducer
                    )
            )
        }

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            val width = getMaxMeasurement(widthMeasureSpec, 0)
            val height = getMaxMeasurement(heightMeasureSpec, 0)
            super.onMeasure(
                    makeMeasureSpec(width, MeasureSpec.EXACTLY),
                    makeMeasureSpec(height, MeasureSpec.EXACTLY)
            )
        }

    }

    init {

        showView(
                view = waiterViewContainer,
                animationTime = TimeValue.ZERO
        )

        CountriesMapManager.countriesMap.get(context).await { countriesMap ->
            showView(
                    view = MapView(
                            context = context,
                            countriesInfo = countriesInfo,
                            countriesMap = countriesMap
                    )
            )
        }

    }


}