package ru.hnau.statisticclient

import android.app.Activity
import android.os.Bundle
import ru.hnau.androidutils.ui.view.pager.Pager
import ru.hnau.androidutils.ui.view.utils.setBackgroundColor
import ru.hnau.statisticclient.pages.main.MainPage
import ru.hnau.statisticclient.utils.ColorManager

class AppActivity : Activity() {

    private val pager by lazy {
        Pager(
                context = this,
                initialPageGetter = { MainPage() },
                pagesStack = MainPagerPagesStack
        ).apply {
            setBackgroundColor(ColorManager.BG_DARK)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(pager)
    }

    override fun onBackPressed() {
        if (pager.handleGoBack()) {
            return
        }
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        pager.release()
    }


}
