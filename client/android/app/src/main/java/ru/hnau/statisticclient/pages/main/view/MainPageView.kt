package ru.hnau.statisticclient.pages.main.view

import android.annotation.SuppressLint
import android.content.Context
import android.support.design.widget.TabLayout
import android.view.Gravity
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import ru.hnau.statisticclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class MainPageView(
        context: Context,
        onCountrySelected: (Country) -> Unit,
        onCategorySelected: (Category) -> Unit
) : LinearLayout(
        context
) {

    private val viewPager = MainPageViewPager(
            context,
            onCountrySelected,
            onCategorySelected
    ).apply {
        setLinearLayoutLayoutParams(MATCH_PARENT, 0, 1f)
    }

    private val tabLayout = TabLayout(context).apply {
        setTabTextColors(
                ColorManager.FG.get(context),
                ColorManager.PRIMARY.get(context)
        )
        setSelectedTabIndicatorColor(ColorManager.PRIMARY.get(context))
        setupWithViewPager(viewPager)
        tabMode = TabLayout.MODE_SCROLLABLE
        tabGravity = Gravity.CENTER
        layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            setBottomMargin(context, DpPxGetter.fromDp(4))
        }
    }

    init {
        orientation = VERTICAL
        gravity = Gravity.CENTER
        addView(tabLayout)
        addView(viewPager)
    }

}