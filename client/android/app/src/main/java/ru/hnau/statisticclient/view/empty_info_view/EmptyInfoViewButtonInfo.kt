package ru.hnau.statisticclient.view.empty_info_view

import ru.hnau.androidutils.context_getters.StringGetter


data class EmptyInfoViewButtonInfo(
        val title: StringGetter,
        val onClick: () -> Unit
)