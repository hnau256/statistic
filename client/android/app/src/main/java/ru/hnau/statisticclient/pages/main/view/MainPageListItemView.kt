package ru.hnau.statisticclient.pages.main.view

import android.content.Context
import android.view.Gravity
import android.view.View
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.base_list.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.clickable.ClickableLinearLayout
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLinearLayoutLayoutParams
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.androidutils.ui.view.utils.setRightPadding
import ru.hnau.statisticclient.utils.ColorManager


class MainPageListItemView(
        context: Context
) : ClickableLinearLayout(
        context = context,
        rippleDrawInfo = ColorManager.DEFAULT_RIPPLE_DRAW_INFO
), BaseListViewWrapper<MainPageListItem> {

    override val view: View
        get() = this

    private val titleView = Label(
            context = context,
            textColor = ColorManager.FG,
            gravity = HGravity.LEFT_CENTER_VERTICAL,
            textSize = DpPxGetter.fromDp(16)
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f)
        setRightPadding(DpPxGetter.fromDp(8))
    }

    private val infoView = MainPageListItemInfoView(context)

    init {
        orientation = HORIZONTAL
        setPadding(DpPxGetter.fromDp(8), DpPxGetter.fromDp(8))
        gravity = Gravity.CENTER

        addView(titleView)
        addView(infoView)
    }

    override fun setContent(content: MainPageListItem) {
        titleView.text = content.title
        infoView.text = content.info
        setOnClickListener { content.onClick.invoke() }
    }

}