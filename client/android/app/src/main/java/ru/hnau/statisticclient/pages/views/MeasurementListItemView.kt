package ru.hnau.statisticclient.pages.views

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DpPxGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.base_list.BaseListViewWrapper
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.statistic_common.measurement.Measurement
import ru.hnau.statisticclient.utils.ColorManager


@SuppressLint("ViewConstructor")
class MeasurementListItemView(
        context: Context,
        private val measurementToTileConverter: (Measurement) -> String
) : LinearLayout(
        context
), BaseListViewWrapper<Measurement> {

    override val view: View
        get() = this

    private val titleView = Label(
            context = context,
            textColor = ColorManager.FG,
            gravity = HGravity.LEFT_CENTER_VERTICAL
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f)
        setRightPadding(DpPxGetter.fromDp(8))
    }

    private val valueView = Label(
            context = context,
            textColor = ColorManager.PRIMARY,
            gravity = HGravity.RIGHT_CENTER_VERTICAL
    ).apply {
        setLinearLayoutLayoutParams(0, WRAP_CONTENT, 1f)
    }

    init {
        orientation = HORIZONTAL
        gravity = Gravity.CENTER
        setBackgroundColor(ColorManager.BG)
        setPadding(
                horizontal = DpPxGetter.fromDp(16),
                vertical = DpPxGetter.fromDp(16)
        )
        addView(titleView)
        addView(valueView)
    }

    override fun setContent(content: Measurement) {
        titleView.text = StringGetter(measurementToTileConverter.invoke(content))
        valueView.text = StringGetter(content.value + " " + content.category.dimension.title)
    }
}