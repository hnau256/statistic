package ru.hnau.statisticclient.utils

import android.content.Context
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import ru.hnau.androidutils.utils.mapUi
import ru.hnau.jutils.cached_value.CachedValue
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.finisher.NewThreadFinisher
import ru.hnau.statistic_common.map.CountriesMap
import ru.hnau.statisticclient.R
import java.io.InputStreamReader


object CountriesMapManager {

    private const val MAP_JSON_RES_ID = R.raw.countries_map_info

    val countriesMap = CachedValue<Context, Finisher<CountriesMap>> { context ->
        NewThreadFinisher.sync {
            val res = context.resources
            val inputStream = res.openRawResource(MAP_JSON_RES_ID)
            val inputStringReader = InputStreamReader(inputStream)
            val jsonReader = JsonReader(inputStringReader)
            Gson().fromJson<CountriesMap>(jsonReader, CountriesMap::class.java)
        }.mapUi()
    }

}