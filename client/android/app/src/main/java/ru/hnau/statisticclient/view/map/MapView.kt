package ru.hnau.statisticclient.view.map

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.utils.drawing.doInState
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.map.*
import ru.hnau.statisticclient.R
import kotlin.math.max


@SuppressLint("ViewConstructor")
class MapView(
        context: Context,
        private val countriesInfo: Map<Country, MapViewCountryInfo>,
        private val countriesMap: CountriesMap
) : SceneView(
        context = context,
        maxScale = 1000f,
        sceneWidth = countriesMap.size.x,
        sceneHeight = countriesMap.size.y
) {

    companion object {

        private val BACKGROUND_COLOR = ColorGetter.byResId(R.color.map_background)
        private val DEFAULT_COUNTRY_FILL_COLOR = ColorGetter.byResId(R.color.map_country_fill)
        private val DEFAULT_COUNTRY_STROKE_COLOR = ColorGetter.byResId(R.color.map_country_stroke)

    }

    private val backgroundPaint = Paint().apply {
        color = BACKGROUND_COLOR.get(context)
    }

    private val path = Path()

    private val countryFillPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val countryStrokePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = DEFAULT_COUNTRY_STROKE_COLOR.get(context)
    }

    init {
        setSoftwareRendering()
    }

    override fun drawScene(canvas: Canvas, viewport: RectF) {

        countryStrokePaint.strokeWidth = max(viewport.width(), viewport.height()) * 0.0005f
        val viewRect = MapRect(
                left = viewport.left,
                top = viewport.top,
                right = viewport.right,
                bottom = viewport.bottom
        )
        countriesMap.countriesPaths.forEach {
            drawCountry(canvas, viewRect, it.value, it.key)
        }
    }

    override fun draw(canvas: Canvas) {
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), backgroundPaint)
        super.draw(canvas)
    }

    private fun drawCountry(canvas: Canvas, viewRect: MapRect, mapCountryPaths: MapPaths, country: Country) {

        val countryDrawInfo = countriesInfo[country]
        countryFillPaint.color = (countryDrawInfo?.color ?: DEFAULT_COUNTRY_FILL_COLOR).get(context)

        mapCountryPaths.paths.forEach {
            val outerRect = it.outerRect ?: return@forEach
            if (rectCrossRect(viewRect, outerRect)) {
                mapPathToPath(it, path)
                canvas.drawPath(path, countryFillPaint)
                canvas.drawPath(path, countryStrokePaint)
            }
        }
        val circle = mapCountryPaths.circle ?: return
        if (circle.radius > 0 && circleCrossRect(circle, viewRect)) {
            val countryDrawable = countryDrawInfo?.drawable
            if (countryDrawable != null) {
                val scale = circle.radius / MapDrawable.BASE_SCALE
                canvas.doInState {
                    translate(circle.center.x, circle.center.y)
                    scale(scale, scale)
                    countryDrawable.draw(canvas)
                }
            }
        }
    }

    private fun mapPathToPath(mapPath: MapPath, path: Path) {
        path.reset()
        if (mapPath.points.size < 3) {
            return
        }
        path.moveTo(mapPath.points[0].x, mapPath.points[0].y)
        mapPath.points.forEachIndexed { i, point ->
            if (i > 0) {
                path.lineTo(point.x, point.y)
            }
        }
        path.close()
    }

    private fun rectCrossRect(rect1: MapRect, rect2: MapRect): Boolean {
        val crossX = (rect1.left >= rect2.left && rect1.left <= rect2.right) ||
                (rect1.right >= rect2.left && rect1.right <= rect2.right) ||
                (rect2.left >= rect1.left && rect2.left <= rect1.right) ||
                (rect2.right >= rect1.left && rect2.right <= rect1.right)
        if (!crossX) {
            return false
        }
        return (rect1.top >= rect2.top && rect1.top <= rect2.bottom) ||
                (rect1.bottom >= rect2.top && rect1.bottom <= rect2.bottom) ||
                (rect2.top >= rect1.top && rect2.top <= rect1.bottom) ||
                (rect2.bottom >= rect1.top && rect2.bottom <= rect1.bottom)
    }

    private fun circleCrossRect(circle: MapCircle, rect: MapRect) =
            circle.center.x > rect.left - circle.radius && circle.center.x < rect.right + circle.radius &&
                    circle.center.y > rect.top - circle.radius && circle.center.y < rect.bottom + circle.radius

}