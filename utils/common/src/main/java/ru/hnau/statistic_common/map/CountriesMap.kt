package ru.hnau.statistic_common.map

import ru.hnau.statistic_common.country.Country


data class CountriesMap(
        val size: MapPoint,
        val countriesPaths: Map<Country, MapPaths>
)