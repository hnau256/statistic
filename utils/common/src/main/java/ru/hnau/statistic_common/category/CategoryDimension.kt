package ru.hnau.statistic_common.category


enum class CategoryDimension(
        val title: String
) {

    COUNT("шт."),
    KM("км"),
    DOLLAR("$"),
    DOLLAR_AT_YEAR("$/год"),
    TONS_AT_YEAR("тонн/год"),
    BBL("баррелей"),
    BBL_AT_DAY("баррелей/день"),
    PEOPLE("чел."),
    PEOPLE_AT_YEAR("чел./год"),
    KWH("КВт/ч"),
    YEAR("лет"),
    BIRTHS_PER_WOMAN("родов/жен."),
    PERCENTAGE("%"),
    PERCENTAGE_OF_GDP("% ВВП"),
    PEOPLE_PER_1000_PEOPLE_AT_YEAR("чел. на 1000 чел./год"),
    PEOPLE_PER_1000_PEOPLE("чел. на 1000 чел."),
    PEOPLE_PER_1000_BIRTHED("чел. на 1000 родившихся"),
    PEOPLE_PER_100000_LIVE_BIRTHS("чел. на 100000 родов"),
    KM2("км²"),
    M3("м³")

}