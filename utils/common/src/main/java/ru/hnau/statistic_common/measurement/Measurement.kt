package ru.hnau.statistic_common.measurement

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import utils.Utils

@JsonIgnoreProperties(value = ["description", "value"])
data class Measurement(
        val longValue: Long? = null,
        val floatValue: Float? = null,
        val country: Country,
        val category: Category,
        val received: Long,
        val measured: Long? = null
) {

    companion object {

        fun create(
                value: String,
                country: Country,
                category: Category,
                received: Long,
                measured: Long? = null
        ): Measurement {

            val (longValue, FloatValue) = if (category.realValue) {
                null to value.toFloat()
            } else {
                value.toLong() to null
            }

            return Measurement(
                    longValue = longValue,
                    floatValue = FloatValue,
                    category = category,
                    country = country,
                    received = received,
                    measured = measured
            )

        }

    }

    val value: String
        get() =
            if (category.realValue)
                ((floatValue ?: 0.0).toString())
            else
                ((longValue ?: 0L).toString())

    fun getDescription() = "Страна: ${country.nameRu}, " +
            "Категория: ${category.title}, " +
            "Значение: ${value + category.dimension.title}, " +
            "Получено: ${Utils.timeToStr(received)}, " +
            "Измерено: ${measured?.let { Utils.timeToStr(it) } ?: "неизвестно"}"


}