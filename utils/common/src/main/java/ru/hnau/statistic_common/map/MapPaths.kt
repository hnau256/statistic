package ru.hnau.statistic_common.map

data class MapPaths(
        val paths: List<MapPath>,
        val circle: MapCircle?
)