package ru.hnau.statistic_common.map


data class MapCircle(
        val center: MapPoint,
        val radius: Float
)