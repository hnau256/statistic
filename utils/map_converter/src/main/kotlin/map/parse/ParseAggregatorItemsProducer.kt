package map.parse


interface ParseAggregatorItemsProducer<T> {

    fun getNextItem(): T?


}