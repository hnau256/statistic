package map.parse

import map.parse.command.SvgPathCloseCommand
import map.parse.command.base.SvgPathCommand
import ru.hnau.statistic_common.map.MapPoint
import java.util.*


class SvgPartCommandsAggregator(
        producer: ParseAggregatorItemsProducer<SvgPathCommand>
) : ParseAggregator<SvgPathCommand, List<MapPoint>>(
        producer
) {

    private var lastPartFinished = MapPoint(0f, 0f)


    override fun aggregate(collectedItems: List<SvgPathCommand>, nextItem: SvgPathCommand?): List<MapPoint>? {
        if (nextItem != null && nextItem !is SvgPathCloseCommand) {
            return null
        }

        val commands = collectedItems.dropWhile { it is SvgPathCloseCommand }
        if (commands.isEmpty()) {
            return null
        }

        val startPos = lastPartFinished
        var currentPos = startPos

        val path = LinkedList<MapPoint>()

        val firstCommand = commands.firstOrNull()
        if (firstCommand?.line != false) {
            path.add(startPos)
        }

        commands.forEach {
            currentPos = it.calcNewPos(currentPos) ?: startPos
            path.add(currentPos)
        }

        lastPartFinished = currentPos
        return path
    }

}