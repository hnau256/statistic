package map.parse

import java.util.*


abstract class ParseAggregator<I, O>(
        private val parseAggregatorItemsProducer: ParseAggregatorItemsProducer<I>
) : ParseAggregatorItemsProducer<O> {

    private val collectedItems = LinkedList<I>()

    private var finished = false

    @Throws(ParseAggregatorException::class)
    protected abstract fun aggregate(collectedItems: List<I>, nextItem: I?): O?

    override fun getNextItem(): O? {
        if (finished) {
            return null
        }

        while (true) {
            val nextItem = parseAggregatorItemsProducer.getNextItem()

            val aggregatedItem = aggregate(collectedItems, nextItem)
            if (nextItem == null) {
                finished = true
                return aggregatedItem
            } else {
                if (aggregatedItem != null) {
                    collectedItems.clear()
                }
                collectedItems.add(nextItem)
                if (aggregatedItem != null) {
                    return aggregatedItem
                }
            }

        }
    }

}