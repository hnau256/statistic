package map.parse.symbol


enum class SvgPathSymbolType {

    MOVE,
    MOVE_ABSOLUTE,
    LINE,
    LINE_ABSOLUTE,
    HORIZONTAL_LINE,
    HORIZONTAL_LINE_ABSOLUTE,
    VERTICAL_LINE,
    VERTICAL_LINE_ABSOLUTE,
    NUMBER,
    CLOSE

}