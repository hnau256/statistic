package map.parse.symbol


data class SvgPathSymbol(
        val type: SvgPathSymbolType,
        val value: Float? = null
)