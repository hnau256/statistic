package map.parse

import java.util.*


object ParseCollector {

    fun <T> collect(producer: ParseAggregatorItemsProducer<T>): List<T> {
        val result = LinkedList<T>()
        while (true) {
            val item = producer.getNextItem()
            if (item == null) {
                return result
            } else {
                result.add(item)
            }
        }
    }

}