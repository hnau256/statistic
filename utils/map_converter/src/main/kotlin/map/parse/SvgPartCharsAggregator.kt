package map.parse

import map.parse.symbol.SvgPathSymbol
import map.parse.symbol.SvgPathSymbolType


class SvgPartCharsAggregator(
        producer: ParseAggregatorItemsProducer<Char>
) : ParseAggregator<Char, SvgPathSymbol>(
        producer
) {

    companion object {

        private val STOP_CHARS = arrayOf(null, ',', ' ')

    }

    @Throws(ParseAggregatorException::class)
    override fun aggregate(collectedItems: List<Char>, nextItem: Char?): SvgPathSymbol? {
        if (nextItem !in STOP_CHARS) {
            return null
        }
        val significantChars = collectedItems.takeLastWhile { it !in STOP_CHARS }.joinToString(separator = "")
        val firstChar = significantChars.firstOrNull() ?: return null
        return when (firstChar) {
            'l' -> SvgPathSymbol(SvgPathSymbolType.LINE)
            'L' -> SvgPathSymbol(SvgPathSymbolType.LINE_ABSOLUTE)
            'm' -> SvgPathSymbol(SvgPathSymbolType.MOVE)
            'M' -> SvgPathSymbol(SvgPathSymbolType.MOVE_ABSOLUTE)
            'h' -> SvgPathSymbol(SvgPathSymbolType.HORIZONTAL_LINE)
            'H' -> SvgPathSymbol(SvgPathSymbolType.HORIZONTAL_LINE_ABSOLUTE)
            'v' -> SvgPathSymbol(SvgPathSymbolType.VERTICAL_LINE)
            'V' -> SvgPathSymbol(SvgPathSymbolType.VERTICAL_LINE_ABSOLUTE)
            'z', 'Z' -> SvgPathSymbol(SvgPathSymbolType.CLOSE)
            else -> {
                val value = significantChars.toFloatOrNull()
                        ?: throw ParseAggregatorException("Undefined symbol $significantChars")
                SvgPathSymbol(SvgPathSymbolType.NUMBER, value)
            }
        }
    }

}