package map.parse.command

import ru.hnau.statistic_common.map.MapPoint

class SvgPathVerticalLineCommand(
        dd: Float
) : SvgPathLineCommand(
        MapPoint(0f, dd)
)