package map.parse.command

import map.parse.command.base.SvgPathRelativePointCommand
import ru.hnau.statistic_common.map.MapPoint


class SvgPathMoveCommand(
        dp: MapPoint
) : SvgPathRelativePointCommand(
        dp,
        false
)