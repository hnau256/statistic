package map.parse.command.base

import ru.hnau.statistic_common.map.MapPoint


abstract class SvgPathRelativePointCommand(
        p: MapPoint,
        line: Boolean
) : SvgPathPointCommand(
        p,
        line
) {

    override fun calcNewPos(oldPos: MapPoint) = oldPos + p

}