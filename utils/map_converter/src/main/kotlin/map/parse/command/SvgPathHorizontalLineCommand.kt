package map.parse.command


import ru.hnau.statistic_common.map.MapPoint

class SvgPathHorizontalLineCommand(
        dd: Float
) : SvgPathLineCommand(
        MapPoint(dd, 0f)
)