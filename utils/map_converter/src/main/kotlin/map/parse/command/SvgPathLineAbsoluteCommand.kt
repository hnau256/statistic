package map.parse.command

import map.parse.command.base.SvgPathAbsolutePointCommand
import ru.hnau.statistic_common.map.MapPoint

open class SvgPathLineAbsoluteCommand(
        p: MapPoint
) : SvgPathAbsolutePointCommand(
        p,
        true
)