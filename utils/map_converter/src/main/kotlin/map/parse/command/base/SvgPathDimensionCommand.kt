package map.parse.command.base




abstract class SvgPathDimensionCommand(
        val d: Float,
        line: Boolean
) : SvgPathCommand(
        line
) {

    override fun getParams() = listOf(d)

}