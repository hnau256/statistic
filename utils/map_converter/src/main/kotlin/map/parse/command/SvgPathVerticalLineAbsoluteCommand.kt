package map.parse.command


import map.parse.command.base.SvgPathDimensionCommand
import ru.hnau.statistic_common.map.MapPoint


class SvgPathVerticalLineAbsoluteCommand(
        d: Float
) : SvgPathDimensionCommand(
        d,
        true
) {

    override fun calcNewPos(oldPos: MapPoint) = MapPoint(oldPos.x, d)

}