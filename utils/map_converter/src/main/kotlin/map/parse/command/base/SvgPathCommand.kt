package map.parse.command.base

import ru.hnau.statistic_common.map.MapPoint

abstract class SvgPathCommand(
        val line: Boolean
) {

    abstract fun calcNewPos(oldPos: MapPoint): MapPoint?

    protected abstract fun getParams(): List<Float>

    override fun toString() = "${this.javaClass.simpleName}(${getParams().joinToString()})"

}