package map.parse.command

import map.parse.command.base.SvgPathRelativePointCommand
import ru.hnau.statistic_common.map.MapPoint

open class SvgPathLineCommand(
        dp: MapPoint
) : SvgPathRelativePointCommand(
        dp,
        true
)