package map.parse.command

import map.parse.command.base.SvgPathAbsolutePointCommand
import ru.hnau.statistic_common.map.MapPoint

class SvgPathMoveAbsoluteCommand(
        p: MapPoint
) : SvgPathAbsolutePointCommand(
        p,
        false
)