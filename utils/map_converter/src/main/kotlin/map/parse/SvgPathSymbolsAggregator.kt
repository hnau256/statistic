package map.parse


import map.parse.command.*
import map.parse.command.base.SvgPathCommand
import map.parse.symbol.SvgPathSymbol
import map.parse.symbol.SvgPathSymbolType
import ru.hnau.statistic_common.map.MapPoint


class SvgPathSymbolsAggregator(
        producer: ParseAggregatorItemsProducer<SvgPathSymbol>
) : ParseAggregator<SvgPathSymbol, SvgPathCommand>(
        producer
) {

    companion object {

        private val requiredParamsCount = hashMapOf(
                SvgPathSymbolType.LINE_ABSOLUTE to 2,
                SvgPathSymbolType.LINE to 2,
                SvgPathSymbolType.MOVE_ABSOLUTE to 2,
                SvgPathSymbolType.MOVE to 2,
                SvgPathSymbolType.HORIZONTAL_LINE_ABSOLUTE to 1,
                SvgPathSymbolType.HORIZONTAL_LINE to 1,
                SvgPathSymbolType.VERTICAL_LINE_ABSOLUTE to 1,
                SvgPathSymbolType.VERTICAL_LINE to 1
        )

    }

    private var lastCommandSymbolType = SvgPathSymbolType.MOVE_ABSOLUTE

    override fun aggregate(collectedItems: List<SvgPathSymbol>, nextItem: SvgPathSymbol?): SvgPathCommand? {

        val firstTypeOrNull = collectedItems.firstOrNull()?.type
        val firstIsNumber = firstTypeOrNull == SvgPathSymbolType.NUMBER
        val type = firstTypeOrNull?.takeIf { !firstIsNumber } ?: lastCommandSymbolType
        val paramsCount = collectedItems.size - (if (firstIsNumber) 0 else 1)
        val requiredParamsCount = SvgPathSymbolsAggregator.requiredParamsCount[type] ?: 0

        if (paramsCount < requiredParamsCount) {
            return null
        }

        if (paramsCount > requiredParamsCount) {
            throw ParseAggregatorException("Required params count is $requiredParamsCount, but found $paramsCount")
        }

        val paramsSymbols = if (firstIsNumber) collectedItems else collectedItems.drop(1)

        val params: List<Float> = paramsSymbols.map {
            if (it.type != SvgPathSymbolType.NUMBER) {
                throw ParseAggregatorException("Required number but found command")
            }
            val value = it.value ?: throw ParseAggregatorException("Undefined error")
            value
        }

        lastCommandSymbolType = type
        return when (type) {
            SvgPathSymbolType.MOVE -> SvgPathMoveCommand(MapPoint(params[0], params[1]))
            SvgPathSymbolType.MOVE_ABSOLUTE -> SvgPathMoveAbsoluteCommand(MapPoint(params[0], params[1]))
            SvgPathSymbolType.LINE -> SvgPathLineCommand(MapPoint(params[0], params[1]))
            SvgPathSymbolType.LINE_ABSOLUTE -> SvgPathLineAbsoluteCommand(MapPoint(params[0], params[1]))
            SvgPathSymbolType.HORIZONTAL_LINE -> SvgPathHorizontalLineCommand(params[0])
            SvgPathSymbolType.HORIZONTAL_LINE_ABSOLUTE -> SvgPathHorizontalLineAbsoluteCommand(params[0])
            SvgPathSymbolType.VERTICAL_LINE -> SvgPathVerticalLineCommand(params[0])
            SvgPathSymbolType.VERTICAL_LINE_ABSOLUTE -> SvgPathVerticalLineAbsoluteCommand(params[0])
            SvgPathSymbolType.CLOSE -> SvgPathCloseCommand()
            else -> throw ParseAggregatorException("Undefined error")
        }
    }


}