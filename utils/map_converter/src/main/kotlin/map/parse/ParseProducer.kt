package map.parse

class ParseProducer(
        private val string: String
): ParseAggregatorItemsProducer<Char> {

    private var currentPos = 0

    override fun getNextItem(): Char? {
        val result = string.getOrNull(currentPos)
        currentPos++
        return result
    }

}