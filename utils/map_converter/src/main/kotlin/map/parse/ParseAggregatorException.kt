package map.parse


class ParseAggregatorException(msg: String) : Exception("Parse error: $msg")