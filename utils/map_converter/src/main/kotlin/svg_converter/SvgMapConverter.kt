package svg_converter

import org.w3c.dom.Node
import ru.hnau.statistic_common.country.Country
import javax.xml.parsers.DocumentBuilderFactory


object SvgMapConverter {

    fun convertFileToCountriesWithSvgPaths(fileName: String): Map<Country, String> {
        val documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        val document = documentBuilder.parse(fileName)
        val root = document.documentElement
        val result = HashMap<Country, String>()
        workWithElement(root, result)
        return result
    }

    private fun workWithElement(node: Node, map: MutableMap<Country, String>) {
        if (node.nodeType == Node.TEXT_NODE) {
            return
        }
        if (node.nodeName == "path") {
            val attributes = node.attributes
            var country: Country? = null
            var data: String? = null
            repeat(attributes.length) {
                val attribute = attributes.item(it)
                if (attribute.nodeType != Node.TEXT_NODE) {
                    val name = attribute.nodeName
                    val value = attribute.nodeValue
                    if (name == "id") {
                        country = idToCountry(value)
                    }
                    if (name == "d") {
                        data = value
                    }
                }
            }
            if (country != null && data != null) {
                map[country!!] = data!!
            }
            return
        }
        val children = node.childNodes
        repeat(children.length) {
            workWithElement(children.item(it), map)
        }
    }

    private val SKIPPED_COUNTRIES = hashSetOf(
            "AX",
            "BL",
            "BQ",
            "GG",
            "GO",
            "JE",
            "JU",
            "MF",
            "SX",
            "UM-DQ",
            "UM-FQ",
            "UM-HQ",
            "UM-JQ",
            "UM-MQ",
            "UM-WQ"
    )

    private val COUNTRIES_IDS = hashMapOf(
            "PS" to Country.ISRAEL
    )

    private fun idToCountry(id: String): Country? {
        if (SKIPPED_COUNTRIES.contains(id)) {
            return null
        }
        return COUNTRIES_IDS[id] ?: Country.values().find { it.codeEn2 == id }
                ?: throw RuntimeException("Undefined country: '$id'")
    }

}