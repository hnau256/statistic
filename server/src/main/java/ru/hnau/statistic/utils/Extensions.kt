package ru.hnau.statistic.utils


fun Throwable?.messageOrUndefined() =
        this?.message ?: "Undefined error"