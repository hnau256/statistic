package ru.hnau.statistic.utils

import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.logging.log4j.LogManager
import ru.hnau.jutils.possible.Possible
import java.io.IOException


object SimpleLoader {

    private val logger = LogManager.getLogger(this)!!

    private val client = OkHttpClient()

    fun load(path: String): Possible<ByteArray> {
        val start = System.currentTimeMillis()
        logger.debug("Loading data from $path")
        val request = Request.Builder().url(path).build()

        return try {
            val bytes = client.newCall(request).execute().body()?.bytes() ?: byteArrayOf()
            val time = Utils.timeToStr(System.currentTimeMillis() - start, false)
            logger.debug("Success loaded data from $path, (time: $time)")
            Possible.success(bytes)
        } catch (ex: IOException) {
            val message = "Error while loading data from $path: ${ex.message}"
            logger.warn(message)
            Possible.error(message)
        }

    }

}