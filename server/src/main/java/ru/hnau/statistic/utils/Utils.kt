package ru.hnau.statistic.utils

import java.util.*


object Utils {

    fun timeToStr(timeMs: Long, useDate: Boolean = true): String {
        val calendar = Calendar.getInstance(if (useDate) TimeZone.getDefault() else TimeZone.getTimeZone(""))
        calendar.timeInMillis = timeMs
        val dateStr = if (!useDate) "" else
            cutStrToLength(calendar.get(Calendar.DAY_OF_MONTH), 2) + "." +
                    cutStrToLength(calendar.get(Calendar.MONTH), 2) + "." +
                    cutStrToLength(calendar.get(Calendar.YEAR), 4) + " "
        val timeStr = cutStrToLength(calendar.get(Calendar.HOUR_OF_DAY), 2) + ":" +
                cutStrToLength(calendar.get(Calendar.MINUTE), 2) + ":" +
                cutStrToLength(calendar.get(Calendar.SECOND), 2) + "." +
                cutStrToLength(calendar.get(Calendar.MILLISECOND), 3)
        return dateStr + timeStr
    }

    private fun cutStrToLength(strInt: Int, length: Int) = cutStrToLength(strInt.toString(), length)

    private fun cutStrToLength(str: String, length: Int): String {
        val len = str.length
        val count = length - len
        val prefix = (0 until count).joinToString(separator = "", transform = { "0" })
        return prefix + str
    }

}