package ru.hnau.statistic.data.update

import ru.hnau.jutils.finisher.Finisher
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.measurement.Measurement


interface CategoryMeasurementsReceiver {

    val supportedCategories: List<Category>

    fun getMeasurements(category: Category): Finisher<List<Measurement>>

}