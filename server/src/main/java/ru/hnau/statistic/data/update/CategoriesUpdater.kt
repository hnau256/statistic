package ru.hnau.statistic.data.update

import ru.hnau.statistic.data.update.cis.CiaMeasurementsReceiver
import ru.hnau.statistic_common.category.Category


object CategoriesUpdater {

    private val updaters = arrayOf(CiaMeasurementsReceiver)

    private val categoryUpdaters =
            updaters
                    .map { updater ->
                        updater.supportedCategories.map { category -> category to updater }
                    }
                    .flatten()
                    .associate { it }

    fun getUpdater(category: Category) = categoryUpdaters[category]

}