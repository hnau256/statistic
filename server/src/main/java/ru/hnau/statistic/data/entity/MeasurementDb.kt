package ru.hnau.statistic.data.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country
import ru.hnau.statistic_common.measurement.Measurement

@Document(collection = "StatisticMeasurement")
data class MeasurementDb(
        @Id val id: String? = null,
        val longValue: Long? = null,
        val floatValue: Float? = null,
        val country: Country? = null,
        val category: Category? = null,
        val received: Long? = null,
        val measured: Long? = null
) {

    constructor(measurement: Measurement) : this(
            id = "${measurement.category}_${measurement.country}_${measurement.received}",
            category = measurement.category,
            country = measurement.country,
            measured = measurement.measured,
            received = measurement.received,
            longValue = measurement.longValue,
            floatValue = measurement.floatValue
    )

    fun toMeasurement() = Measurement(
            country = country ?: Country.AFGHANISTAN,
            category = category ?: Category.AREA,
            longValue = longValue,
            floatValue = floatValue,
            received = received ?: 0L,
            measured = measured
    )

}