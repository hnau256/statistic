package ru.hnau.statistic

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import ru.hnau.statistic.data.entity.MeasurementDbRepository
import ru.hnau.statistic_common.country.Country


@SpringBootApplication
open class Application : CommandLineRunner {

    private val logger = LogManager.getLogger(Application::class.java)

    @Autowired
    lateinit var repository: MeasurementDbRepository

    override fun run(vararg args: String?) {

    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Application::class.java, *args)
        }
    }

}