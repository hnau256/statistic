package ru.hnau.statistic.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.hnau.statistic.data.entity.MeasurementDb
import ru.hnau.statistic.managers.MeasurementsManager
import ru.hnau.statistic_common.category.Category
import ru.hnau.statistic_common.country.Country

@RestController
class RestService {


    @Autowired
    lateinit var measurementsManager: MeasurementsManager

    @RequestMapping("/api")
    fun index() = "Statistic service"

    @RequestMapping("/api/country-info")
    fun countryInfo(@RequestParam(value = "country") country: Country) =
            measurementsManager.getCountryMeasurements(country).map(MeasurementDb::toMeasurement)

    @RequestMapping("/api/category-info")
    fun countryInfo(@RequestParam(value = "category") category: Category) =
            measurementsManager.getCategoryMeasurements(category).map(MeasurementDb::toMeasurement)

    @RequestMapping("/api/update")
    fun update() =
            measurementsManager.update()

    @RequestMapping("/api/clear")
    fun clear() =
            measurementsManager.clear()

}